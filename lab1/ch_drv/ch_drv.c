#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/slab.h>




typedef struct node {
    int value;
    struct node* next;
} node_t;

static dev_t first;
static struct cdev c_dev;
static struct class *cl;

static node_t* list;

static struct proc_dir_entry* proc_file;
static size_t number_of_symbols = 0;
static char str[256] = "";


static int my_open(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: open()\n");
  return 0;
}

static int my_close(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: close()\n");
  return 0;
}

static int my_dev_uevent(struct device *dev, struct kobj_uevent_env *env)
{
  add_uevent_var(env, "DEVMODE=%#o", 0666);
  return 0;
}

static ssize_t my_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  printk(KERN_INFO "Driver: read()\n");
//  printk(KERN_INFO "Number of symbols: %d", list->val);

  node_t* now = list;
  int i = 0;
  while(now != NULL){
    printk(KERN_INFO "%d: %d", i, now->value);
    now = now->next;
    i++;
  }
  return 0;
}

static ssize_t my_write(struct file *f, const char __user *buf,  size_t len, loff_t *off)
{
  printk(KERN_INFO "Driver: write()\n");
  node_t* new_node = kmalloc(sizeof (struct node), 1);
  new_node->value = len - 1;
  new_node->next = list;
  list = new_node;
  return len;
}

static ssize_t procfile_read(struct file *f, char __user *buf, size_t len, loff_t *offset)
{
  printk(KERN_INFO "ProcFile: read()\n");
  snprintf(str, sizeof str, "%zu\n", number_of_symbols);
  if (*offset >= len || copy_to_user(buf, str, sizeof(str)))
    {
        pr_info("failed copy_to_user\n");
        return 0;
    }
  else
    {
        pr_info("ProcFile: read() - %s\n", f->f_path.dentry->d_name.name);
        *offset += len;
        return len;
    }
}

static ssize_t procfile_write(struct file *f, const char __user *buf,  size_t len, loff_t *off)
{
  printk(KERN_INFO "ProcFile: write()\n");
  return -1;
}


static struct file_operations mychdev_fops =
{
  .owner = THIS_MODULE,
  .open = my_open,
  .release = my_close,
  .read = my_read,
  .write = my_write
};

static struct proc_ops proc_fops =
{
  .proc_read = procfile_read,
  .proc_write = procfile_write
};

static int __init ch_drv_init(void)
{
    printk(KERN_INFO "Hello!\n");
    list = NULL;
    if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0)
	  {
		return -1;
	  }
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL)
	  {
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    cl->dev_uevent = my_dev_uevent;
    if (device_create(cl, NULL, first, NULL, "mychdev") == NULL)
	  {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    cdev_init(&c_dev, &mychdev_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
	  {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    proc_file = proc_create("var1", 0644, NULL, &proc_fops);
    printk(KERN_INFO "%s: proc file is created\n", THIS_MODULE->name);
    return 0;
}

static void __exit ch_drv_exit(void)
{
    proc_remove(proc_file);
    printk(KERN_INFO "%s: proc file is deleted\n", THIS_MODULE->name);
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    printk(KERN_INFO "Bye!!!\n");
}

module_init(ch_drv_init);
module_exit(ch_drv_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Klimenko K. && Elokhov D.");
MODULE_DESCRIPTION("The first lab IO");

