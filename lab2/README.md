# Лабораторная работа 2

**Название:** Разработка драйверов блочных устройств.

**Цель работы:** Получить знания и навыки разработки драйверов блочных устройств для операционной системы Linux.

**Выполнили:**
- Клименко К.
- Елохов Д.

## Вариант

Один первичный раздел размером 10Мбайт и один расширенный раздел, содержащий два логических раздела размером 20Мбайт каждый.

## Инструкция по сборке
```
make all
make insmod
...
make rmmod
make clean
```
## Инструкция пользователя

> Посмотреть разделы устройства
```
sudo fdisk -l /dev/vramdisk
```

> Форматирование и монтирование раздела диска на директорию
```
sudo mkfs.vfat -F16 /dev/vramdisk6
sudo mkdir -p /mnt/vramdisk6
sudo mount /dev/vramdisk6 /mnt/vramdisk6
```

> Демонтирование
```
sudo umount /mnt/vramdisk6
```

> Скорость передачи данных при копировании файлов между разделами виртуального диска
```
sudo dd if=/dev/vramdisk5 of=/dev/vramdisk6 bs=512 count=20480 oflag=direct
```
![vram-vram](1.png)

> Скорость передачи данных при копировании файлов между разделами виртуального и жесткого дисков
```
sudo dd if=/dev/vramdisk5 of=/dev/sda bs=512 count=20480 oflag=direct
```
![vram-sda](2.png)


## Вывод
В ходе выполнения данной лабораторной работы был дописан исходный драйвер блочного устройства, 
изучены команды форматирования разделов диска, монтирования и демонтирования, 
измерения скорости передачи данных между разделами.
