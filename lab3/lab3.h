#pragma once

#include <linux/types.h>
#include <linux/icmp.h>


struct history {
    char * iface;
    char * content;
    size_t content_length;
    struct history * next;
};

struct history * history_new(
        const char * iface,
        const char * content,
        size_t content_length,
        struct history * next
);

void history_delete(struct history * history);

size_t history_length(struct history * history);
size_t history_to_array(struct history * history, struct history ** dest);

size_t history_print(struct history * history, char ** dest);
