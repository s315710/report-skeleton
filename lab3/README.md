# Лабораторная работа 3

**Название:** "Разработка драйверов сетевых устройств"

**Цель работы:** получить знания и навыки разработки драйверов сетевых
интерфейсов для операционной системы Linux.

**Вариант** 1

Трафик, подлежащий перехвату:

- Пакеты протокола ICMP (Internet Control Message Protocol) – только тип 8. Вывести данные.
- Состояние разбора пакетов необходимо выводить в файл в директории /proc

## Инструкция по сборке

```bash
sudo make
```

## Инструкция пользователя

```bash
sudo make insmod
```

```bash
sudo make dmesg
```

```bash
ip a
```

```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f2:2c:ac brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85595sec preferred_lft 85595sec
    inet6 fe80::3598:d9c8:6acf:2337/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
7: lab3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
    link/ether 08:00:27:f2:2c:ac brd ff:ff:ff:ff:ff:ff
    inet6 fe80::40da:7fa2:5176:a000/64 scope link tentative noprefixroute 
       valid_lft forever preferred_lft forever
```

## Примеры использования

```
ping -c 1 -p 6b616b206a6520686f6365747361 -s 14 10.0.2.15
ping -c 1 -p 74726f6563686b75 -s 8 10.0.2.15
ping -c 1 -p 62657a207a6163686f7461 -s 11 10.0.2.15
```

```
cat /proc/lab3
```

```
8. lo: 14 bytes:
Bytes: "kak je hocetsa"
Hex: 6B 61 6B 20 6A 65 20 68 6F 63 65 74 73 61
9. lo: 8 bytes:
Bytes: "troechku"
Hex: 74 72 6F 65 63 68 6B 75
10. lo: 11 bytes:
Bytes: "bez zachota"
Hex: 62 65 7A 20 7A 61 63 68 6F 74 61
Summary: 137 bytes.
```

**Смотрим статистику принятых пакетов**
```
ip -s link show lab3
```

```
7: lab3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN mode DEFAULT group default qlen 1000
    link/ether 08:00:27:f2:2c:ac brd ff:ff:ff:ff:ff:ff
    RX:  bytes packets errors dropped  missed   mcast           
           417      10      0       0       0       0 
    TX:  bytes packets errors dropped carrier collsns           
         45241     255      0       0       0       0
```

