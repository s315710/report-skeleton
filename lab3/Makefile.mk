obj-m += lab3.o

lab3-objs := main.o history.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

insmod:
	sudo insmod lab3.ko

rmmod:
	sudo rmmod lab3.ko

dmesg:
	dmesg
